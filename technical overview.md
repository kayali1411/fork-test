#### MENA Rates Version 1.0

* In this project for the database design we used **Mysql**. The tables relation are based on **One-to-One Relationships**. And the server API is **Apache 2.0**
*  We used **PHP 5.6** for server code and bulid the back-end for the application.
We used native Mysql query to insert and modify the database records, and we use **Prepared Statements** to prevent SQL injections.
* The front-end was bulit on **HTML & JavaScript** with some of  **JQuery**.
To mainpulate the records from the server to front-end we used **AJAX**.
 * Clarify the files content :
    * In the root directory you find three directories.
    * admin directory including the php files for the back-end.
    * includes directory contains :
           *  includes/js for javascript files.
        *  includes/json contain php files for json objects to deliver the records for the front-end.
        *  includes/media for images and logos.
        *  includes/style for css files.
        *  some php files such as functions.php, header.php, footer.php to make the code more structure.
