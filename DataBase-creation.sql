CREATE DATABASE shopgo_mena_rates;

USE shopgo_mena_rates;

CREATE TABLE gateways
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(60) NOT NULL
  );

CREATE TABLE countries
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(60) NOT NULL
  );

CREATE TABLE currencies
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(60) NOT NULL
  );

CREATE TABLE cards
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(60) NOT NULL
  );

CREATE TABLE rates
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gate_id INT(10) NOT NULL,
  setupfee double NOT NULL,
  transactionfee double NOT NULL,
  FOREIGN KEY (gate_id) REFERENCES gateways(id)
  );

CREATE TABLE users
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(60) NOT NULL,
  password VARCHAR(60) NOT NULL
  );

INSERT INTO users(username, password)
VALUES ('admin', '$2y$10$testlgkbnvxkwuerhtbgvegvB/bIS.ZXWHFTU2dIgxGOaZlm8zOua' );

CREATE TABLE gate_card
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gate_id INT(10) NOT NULL,
  option_id INT(10) NOT NULL,
  FOREIGN KEY (gate_id) REFERENCES gateways(id),
  FOREIGN KEY (option_id) REFERENCES cards(id)
  );

CREATE TABLE gate_country
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gate_id INT(10) NOT NULL,
  option_id INT(10) NOT NULL,
  FOREIGN KEY (gate_id) REFERENCES gateways(Id),
  FOREIGN KEY (option_id) REFERENCES countries(id)
  );

CREATE TABLE gate_currency
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gate_id INT(10) NOT NULL,
  option_id INT(10) NOT NULL,
  FOREIGN KEY (gate_id) REFERENCES gateways(id),
  FOREIGN KEY (option_id) REFERENCES currencies(id)
  );

CREATE TABLE rates_gateways_countries
( id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gate_id INT(10) NOT NULL,
  rate_id INT(10) NOT NULL,
  country_id INT(10) NOT NULL,
  FOREIGN KEY (gate_id) REFERENCES gateways(id),
  FOREIGN KEY (rate_id) REFERENCES rates(id),
  FOREIGN KEY (country_id) REFERENCES countries(id)
  );









