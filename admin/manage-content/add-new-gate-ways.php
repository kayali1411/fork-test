<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>Add a new gate ways:</h2>
	<form action="add-new-gate-ways.php?type=gateways" method="post">
		<label>Gateway name:</label>
		<br>
		<input type="text" name="gateway">
		<br><br>

		<div class="side-input">
			<label>Available countries:</label>
			<br>
			<?php 
				$option_country=dispaly_form_content("countries", $connection); 
				echo $option_country[0];
			?>
			<br>
		</div>
		
		<div class="side-input">
			<label>Available currencies:</label>
			<br>
			<?php 
				$option_currency=dispaly_form_content("currencies", $connection); 
				echo $option_currency[0];
			?>
			<br>
		</div>

		<div class="side-input">
			<label>Available cards:</label>
			<br>
			<?php 
				$option_card=dispaly_form_content("cards", $connection); 
				echo $option_card[0];
			?>
			<br>
		</div>

		<input style="display:block;" type="submit" name="submit" value="Save">
		<br>
		<?php
			if ( isset($_POST["submit"]) ) 
			{
				if ( empty($_POST["gateway"]) ) 
				{
					echo "item name is required";
				}
				elseif (strtoupper(Record_Is_Exist("name", $_POST["gateway"], $connection)) == strtoupper($_POST["gateway"])) 
				{
					echo "This item is already exist.";
				}
				else
				{
					$result=array(0,0,0);
					$result_country=array();
					$result_currency=array();
					$result_card=array();
					
					$j=0;
					for ($i=1; $i<count($option_country); $i=$i+2) 
					{ 
						if ( isset($_POST[$option_country[$i]]) ) 
						{
							$result[0]=1;
							$result_country[$j]=$option_country[$i+1];
							$j=$j+1;
						}
					}

					$j=0;
					for ($i=1; $i<count($option_currency); $i=$i+2) 
					{ 
						if ( isset($_POST[$option_currency[$i]]) ) 
						{
							$result[1]=1;
							$result_currency[$j]=$option_currency[$i+1];
							$j=$j+1;
						}
					}

					$j=0;
					for ($i=1; $i<count($option_card); $i=$i+2) 
					{ 
						if ( isset($_POST[$option_card[$i]]) ) 
						{
							$result[2]=1;
							$result_card[$j]=$option_card[$i+1];
							$j=$j+1;
						}
					}

					$sum=0;
					for ($i=0; $i <=2 ; $i++) {$sum = $sum+$result[$i]; }
					if ($sum < 3) 
					{ echo "Please select at least one option from each field."; }
					else
					{ 
						add_new_item($_POST["gateway"], $connection);
						$output="Item has been added. ";
						$output.="<a href=\"add-rates.php?id=";
						$output.=get_last_record("gateways", $connection);
						$output.="&type=".$_GET["type"]."\">Click to add rates</a>";
						echo $output;
						link_items_with_gateway(get_last_record("gateways", $connection), "gate_country", $result_country, $connection);
						link_items_with_gateway(get_last_record("gateways", $connection), "gate_currency", $result_currency, $connection);
						link_items_with_gateway(get_last_record("gateways", $connection), "gate_card", $result_card, $connection);
					}
				}
			}
		?>

	</form>
</div>

<?php 	require('../../includes/footer.php'); ?>