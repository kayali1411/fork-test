function getGatewaysInfo(option)
{
	$.ajax(
	{
		url :"includes/json/payment_info.php",
		type :"GET",
		data :{ para : option },
		success : function(data)
		{


			var outPut1='<div class="col-md-6 col-sm-12 col-xs-12" >';
			for (var i = 0; i < data.length; i+=2) 
			{
				outPut1+='<div class="col-md-12 col-sm-12 col-xs-12 item" >';
				outPut1+='<a class="title" href="#"><h3 class="text-center" >'+data[i].name+'</h3>';
				outPut1+='<hr/></a>';
				outPut1+='<div class="menu" style="display:none">';
				outPut1+=getGatewayContent(data[i].country, "Countries");
				outPut1+=getCountryRates(data[i].rates[0] , data[i].rates[1]);
				outPut1+=getGatewayContent(data[i].currency, "Currencies");
				outPut1+=getGatewayContent(data[i].card, "Cards");
				outPut1+='</div>';
				outPut1+='</div>';
			}
			outPut1+='</div>';

			var outPut2='<div class="col-md-6 col-sm-12 col-xs-12" >';
			for (var i = 1; i < data.length; i+=2) 
			{
				outPut2+='<div class="col-md-12 col-sm-12 col-xs-12 item" >';
				outPut2+='<a class="title" href="#"><h3 class="text-center" >'+data[i].name+'</h3>';
				outPut2+='<hr/></a>';
				outPut2+='<div class="menu" style="display:none">';
				outPut2+=getGatewayContent(data[i].country, "Countries");
				outPut2+=getCountryRates(data[i].rates[0] , data[i].rates[1]);
				outPut2+=getGatewayContent(data[i].currency, "Currencies");
				outPut2+=getGatewayContent(data[i].card, "Cards");
				outPut2+='</div>';
				outPut2+='</div>';
			}
			outPut1+='</div>';

			var outPut=outPut1+outPut2;
			document.getElementById("content").innerHTML=outPut;

		    $(".item").click(function()
		    {
		    	$(this).find(".menu").slideToggle("fast");
		    });

		    $("a.title").click(function(event)
			{
	    	event.preventDefault();
			});
		},
		error: function()
		{
			var message='<h2>No matching result</h2>';
       		document.getElementById("content").innerHTML=message;
       	}
	});
}



function getFilterOption()
{
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() 
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
		{
			var data=JSON.parse(xmlhttp.responseText);
			var outPut='<br><br/><div class="input-group"><label style="display:block">Select your country:</label>';
			outPut+='<select class="form-control" id="select">';
			for (var i = 0; i < data.countries.length; i++) 
			{
				if (i==0) {outPut+='<option selected>Select option</option>';};
				outPut+='<option>'+data.countries[i]+'</option>';
			}
			outPut+='</select>';
			outPut+='<label><br/>Select your payemnt cards:</label><br/>';
			outPut+='<div class="checkbox">';
			for (var i = 0; i < data.cards.length; i++) 
			{
				outPut+='<label><input type="checkbox" name="card" value="'+data.cards[i]+'">'+data.cards[i]+'</label><br/>';
			}
			outPut+='</div>';
			outPut+='<label>Select your currency:</label><br/>';
			outPut+='<div class="radio">'
			for (var i = 0; i < data.currencies.length; i++) 
			{
				outPut+='<label><input type="radio" name="currency" value="'+data.currencies[i]+'">  '+data.currencies[i]+'</label><br/>';
			}
			outPut+='</div></div>';

			document.getElementById("side").innerHTML=outPut;
			getOptions("#side select");
			getOptions("#side input[type=checkbox]");
			getOptions("#side input[type=radio]");
		}
	}

	xmlhttp.open("GET", "includes/json/filter_options.php" , true);
	xmlhttp.send();	
}

function getGatewayContent(data, name)
{
	var count=data.length;
	var outPut='<ul class="col-sm-3 col-xs-3" >';
	outPut+='<li class="text-center"><h4>'+name+'</h4></li>';
	for (var j = 0; j < count; j++) 
	{
		outPut+='<li class="text-center" ><a href="content?data='+data[j]+'&name='+name.toLowerCase()+'">'+data[j]+'</a></li>';
	}
	outPut+='</ul>';
	return outPut;
}


function getCountryRates(data1, data2)
{
	var count=data1.length
	var outPut='<ul class="col-sm-3 col-xs-3" >';
		outPut+='<li class="text-center" ><h4>Rates</h4></li>';
	for (var j = 0; j < count; j++) 
	{
		outPut+='<li class="text-center" >'+data1[j]+' - '+data2[j]+'</li>';
	}
	outPut+='</ul>';
	return outPut;
}

function getOptions (option) 
{
	$(option).change(function()
	{
		var country=$("#select option:selected").val();
		var currency=$("input[name='currency']:checked").val();
		var card=[];

		$.each($("input[name='card']:checked"), function()
		{            
        	card.push($(this).val());
        });

		if (country=='Select option') {country='blanck'};
		if (currency==undefined) {currency='blanck'};
		if (card=='') {card='blanck'};	 
		$.ajax(
		{
			url :"includes/json/filter.php",
			type :"GET",
			data : { country:country , currency:currency , card:card },
			success : function(result) 
			{
				// console.log(result);
				getGatewaysInfo(result);
			},
			error: function()
			{
           		alert('No Result');
           	}
		});
	});

}

function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) 
	{
		vars[key] = value;
	});

	return vars
}

function infoRecords()
{
	var para=getUrlVars();
	$.ajax(
	{
		url :"../includes/json/content_info.php",
		type :"GET",
		data :{ data : para.data , table : para.name },
		success : function(result)
		{
			var outPut='<h2>Gateways that support '+result.name+' are :</h2>';
			outPut+='<br>';
			outPut+='<ul>';
			for (var i = 0; i < result.gateways.length; i++) 
			{
				outPut+='<li>'+result.gateways[i]+'</li>';
			};
			outPut+='</ul>';
			document.getElementById("content").innerHTML=outPut;
			console.log(outPut);
		},
		error: function()
		{
       		alert('No Result!');
       	}
    });
}

