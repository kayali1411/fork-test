<?php

	header("Content-Type: application/json");
	require('../db_connection.php');


	$query="SELECT name ";
	$query.="FROM gateways ";
	$records=mysqli_query($connection, $query);
	$count=0;
	$content_gateways=array();
	while ($result=mysqli_fetch_assoc($records) ) 
		{
			$content_gateways[$count]=$result["name"];
			$count=$count+1;
		}
	mysqli_free_result($records);



	$query="SELECT name ";
	$query.="FROM countries ";
	$records=mysqli_query($connection, $query);
	$count=0;
	$content_countries=array();
	while ($result=mysqli_fetch_assoc($records) ) 
		{
			$content_countries[$count]=$result["name"];
			$count=$count+1;
		}
	mysqli_free_result($records);


	$result=array('gateways' => $content_gateways , 'countries' => $content_countries);

	$json_data=json_encode($result);
	echo $json_data;

?>