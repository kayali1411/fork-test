<?php

	header("Content-Type: application/json");
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');

	$data1=$_GET["country"];
	$data2=$_GET["card"];
	$data3=$_GET["currency"];

	// $data1='blanck';
	// $data2=array('JCB');
	// $data3='blanck';

	if ($data1!='blanck' || $data3!='blanck') 
	{
		$query="SELECT DISTINCT gateways.id ";
		$query.="FROM gateways ";
		$query.="JOIN gate_country ";
		$query.="ON gateways.id=gate_country.gate_id ";
		$query.="JOIN gate_currency ";
		$query.="ON gateways.id=gate_currency.gate_id ";
		$query.="WHERE ( 1=1 ";

		if ($data1!='blanck') 
		{
			$country_id=get_id($data1, "countries", $connection);
			$query.="AND gate_country.option_id={$country_id} ";
		}

		if ($data3!='blanck') 
		{
			$currency_id=get_id($data3, "currencies", $connection);
			$query.="AND gate_currency.option_id={$currency_id} ";
		}

		$query.=")";
		$records=mysqli_query($connection, $query);

		$count=0;
		$result=array();

		while ( $row=mysqli_fetch_assoc($records) ) 
		{
			$result[$count]=$row["id"];
			$count=$count+1;
		}

		$output=array();
		mysqli_free_result($records);
		for ($i=0; $i < count($result); $i++) 
		{ 
			$output[$i]=get_name($result[$i], "gateways", $connection);
		}

		if ($data2!='blanck') 
		{
			$output=array();
			for ($i=0; $i < count($result); $i++) 
			{ 
				$query="SELECT name ";
				$query.="FROM cards ";
				$query.="JOIN gate_card ";
				$query.="ON cards.id=gate_card.option_id ";
				$query.="WHERE gate_card.gate_id=";
				$query.=$result[$i];

				$records=mysqli_query($connection, $query);

				$count=0;
				$result_card=array();

				while ( $row=mysqli_fetch_assoc($records) ) 
				{
					$result_card[$count]=$row["name"];
					$count=$count+1;
				}

				mysqli_free_result($records);

				$test = array_intersect($result_card, $data2);
				if ( count($data2)==count($test) ) 
				{
					$name=get_name($result[$i], "gateways", $connection);
					array_push( $output, $name );
				}
			}
		}
	}

	else if ($data2!='blanck')
	{	
		$result=get_gateways($connection);
		$output=array();
		for ( $i=0; $i < count($result); $i++ ) 
		{
			$id=get_id($result[$i], "gateways", $connection);
			$query="SELECT name ";
			$query.="FROM cards ";
			$query.="JOIN gate_card ";
			$query.="ON cards.id=gate_card.option_id ";
			$query.="WHERE gate_card.gate_id=";
			$query.=$id;

			$records=mysqli_query($connection, $query);

			$count=0;
			$result_card=array();

			while ( $row=mysqli_fetch_assoc($records) ) 
			{
				$result_card[$count]=$row["name"];
				$count=$count+1;
			}

			mysqli_free_result($records);

			$test = array_intersect($result_card, $data2);
			if ( count($data2)==count($test) ) 
			{
				$name=get_name($id, "gateways", $connection);
				array_push( $output, $name );
			}
		}
	}

	else
	{
		$output=0;
	}
	




$json_data=json_encode($output);
echo $json_data;

?>
