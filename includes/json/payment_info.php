<?php

	header("Content-Type: application/json");
	require('../functions.php');
	require('../db_connection.php');

	$info=$_GET["para"];
	if ($info) 
	{
		$gateways=$info;
	}
	else 
	{
		$gateways=get_gateways($connection);
	}

	$count=0;
	$gateway_info=array();

	for ( $i=0; $i < count($gateways) ; $i++ ) 
	{ 
		$country=get_gateway_info($gateways[$i], "countries", "gate_country", $connection);
		$currency=get_gateway_info($gateways[$i], "currencies", "gate_currency", $connection);
		$card=get_gateway_info($gateways[$i], "cards", "gate_card", $connection);

		$setupfee=array();
		$transactionfee=array();

		for ($j=0; $j < count($country); $j++) 
		{ 
			$setupfee[$j]=get_rate_for_country($gateways[$i], $country[$j], "setupfee", $connection);
			$transactionfee[$j]=get_rate_for_country($gateways[$i], $country[$j], "transactionfee", $connection);
			
		}
		
		
		$rates=array($setupfee, $transactionfee);
		
		$gateway_info[$count]=array('name' => $gateways[$i] , 'country' => $country , 'currency' => $currency , 'card' => $card , 'rates' => $rates );
		$count=$count+1;	

	}

	$json_gateway=json_encode($gateway_info);
	echo $json_gateway;

?>
